# Reign Test

To run the app, just create a .env file with the following values:

- `MONGO_USER`
- `MONGO_PASSWORD`

And then run the command

`docker-compose --env-file ./.env up -d`

This will start up client on port 5000 and the server on port 3000, which then pulls articles from the API after startup.
