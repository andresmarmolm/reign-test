import "./App.css";
import ArticleList from "./components/articles/ArticleList";

function App() {
  return (
    <div className="App">
      <header className="app-header">
        <h1 className="title">HN Feed</h1>
        <h2 className="subtitle">
          We <span className="heart">❤️</span> hacker news!
        </h2>
      </header>
      <main className="content">
        <ArticleList></ArticleList>
      </main>
    </div>
  );
}

export default App;
