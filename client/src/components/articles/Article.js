import { useState } from "react";

import "./Article.css";
import TrashIcon from "./TrashIcon";
import { convertDate } from "../../helpers/convertDate";

function Article({ article, deleteArticle }) {
  const [isHovered, setHovered] = useState(false);

  const mouseEnterHandler = (e) => {
    setHovered(true);
  };

  const mouseLeaveHandler = (e) => {
    setHovered(false);
  };

  const title = article.storyTitle || article.title;
  const url = article.storyUrl || article.url;
  const { author, createdAt } = article;

  return title ? (
    <a href={url} target="_blank" rel="noreferrer noopener">
      <article
        className="article"
        onMouseEnter={mouseEnterHandler}
        onMouseLeave={mouseLeaveHandler}
      >
        <div>
          <h1>{title}</h1> <span className="author"> - {author} - </span>
        </div>
        <p>{convertDate(createdAt)}</p>
        <TrashIcon
          display={isHovered}
          deleteArticle={deleteArticle}
        ></TrashIcon>
      </article>
    </a>
  ) : null;
}

export default Article;
