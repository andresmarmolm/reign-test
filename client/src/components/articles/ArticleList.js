import { useState, useEffect } from "react";
import axios from "axios";

import Article from "./Article";
import "./ArticleList.css";

function ArticleList() {
  const [articles, setArticles] = useState(null);
  useEffect(() => {
    async function fetchData() {
      const articlesRequest = await axios.get(`http://localhost:3000/articles`);
      setArticles(articlesRequest.data);
      console.log(articlesRequest);
    }
    fetchData();
  }, []);
  const deleteArticle = (id) => {
    return async function (e) {
      e.preventDefault();
      try {
        await axios.delete(`http://localhost:3000/articles/${id}`);
      } catch (e) {}
      setArticles(articles.filter((article) => article._id !== id));
    };
  };
  return (
    <div>
      {articles ? (
        articles.map((article) => (
          <Article
            key={article._id}
            article={article}
            deleteArticle={deleteArticle(article._id)}
          />
        ))
      ) : (
        <div className="loading">Loading...</div>
      )}
    </div>
  );
}

export default ArticleList;
