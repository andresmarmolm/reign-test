import { DateTime } from "luxon";

export function convertDate(createdAt) {
  const now = DateTime.local();
  const articleDate = DateTime.fromISO(createdAt);

  if (now.hasSame(articleDate, "day")) {
    return articleDate.toLocaleString(DateTime.TIME_24_SIMPLE);
  } else if (now.minus({ days: 1 }).hasSame(articleDate, "day")) {
    return "Yesterday";
  } else {
    return articleDate.toFormat("MMM d");
  }
}
