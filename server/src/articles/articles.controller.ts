import { Controller, Get, Delete, Param } from '@nestjs/common';
import { ArticlesService } from './articles.service';

@Controller('articles')
export class ArticlesController {
  constructor(private articlesService: ArticlesService) {}

  @Get()
  getLatest() {
    return this.articlesService.getLatest();
  }

  @Delete(':id')
  deleteArticle(@Param() params) {
    return this.articlesService.deleteArticle(params.id);
  }
}
