import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesService } from './articles.service';
import { getModelToken } from '@nestjs/mongoose';
import { Article } from './article.schema';
import { internet, hacker, time, random } from 'faker';

describe('ArticlesService', () => {
  let service: ArticlesService;
  const exampleCount = 5;
  function makeExampleObject() {
    return {
      _id: random.number(),
      created_at: time.recent(),
      title: hacker.phrase(),
      author: internet.userName(),
      url: internet.url(),
      deleted: false,
    };
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        {
          provide: getModelToken(Article.name),
          useFactory: () => {
            const inMemoryDb = Array(exampleCount)
              .fill({})
              .map(() => makeExampleObject());
            return {
              find: function () {
                return this;
              },
              sort: function () {
                return this;
              },
              exec: function () {
                return Promise.resolve(inMemoryDb);
              },
              findByIdAndUpdate: function (id, update) {
                const index = inMemoryDb.findIndex((value) => value._id === id);
                return {
                  exec: function () {
                    inMemoryDb[index] = { ...inMemoryDb[index], ...update };
                    return inMemoryDb[index];
                  },
                };
              },
            };
          },
        },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getLatest', () => {
    it('Should return all articles', async () => {
      expect(await service.getLatest()).toHaveLength(exampleCount);
    });
  });

  describe('deleteArticle', () => {
    it('Should properly update an article', async () => {
      const articles = await service.getLatest();
      const article = articles[0];
      await service.deleteArticle(article._id);
      const newArticles = await service.getLatest();

      expect(article).toHaveProperty('deleted', false);
      expect(
        newArticles.find((value) => value._id === article._id),
      ).toHaveProperty('deleted', true);
    });
  });

  describe('formatArticle', () => {
    it('Should properly convert snake_case properties in an object', () => {
      const exampleObject = makeExampleObject();
      expect(service.formatArticle(exampleObject)).toHaveProperty(
        'createdAt',
        exampleObject['created_at'],
      );
      expect(service.formatArticle(exampleObject)).toHaveProperty(
        'title',
        exampleObject['title'],
      );
      expect(service.formatArticle(exampleObject)).toHaveProperty(
        'author',
        exampleObject['author'],
      );
      expect(service.formatArticle(exampleObject)).toHaveProperty(
        'url',
        exampleObject['url'],
      );
    });
  });
});
