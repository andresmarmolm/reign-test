import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  _id: number;

  @Prop()
  createdAt: string;

  @Prop()
  storyTitle: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  storyUrl: string;

  @Prop()
  author: string;

  @Prop({ default: false })
  deleted: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
