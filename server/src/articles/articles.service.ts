import fetch, { Response } from 'node-fetch';
import camelCase from 'camelcase';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument } from './article.schema';
import { ArticlesResponse } from './dto/ArticlesResponse.dto';
import { Cron, CronExpression, Timeout } from '@nestjs/schedule';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
  ) {}

  async getLatest() {
    return await this.articleModel
      .find({ deleted: false })
      .sort({ createdAt: -1 })
      .exec();
  }

  async deleteArticle(id: number) {
    const article = await this.articleModel
      .findByIdAndUpdate(id, { deleted: true }, { useFindAndModify: false })
      .exec();
    return article;
  }

  async updateArticles() {
    const response: Response = await fetch(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    const articles: ArticlesResponse = await response.json();

    const promiseArray = articles.hits.map((article) => {
      article = this.formatArticle(article);
      article._id = article.objectId;
      return this.articleModel.findByIdAndUpdate(article._id, article, {
        upsert: true,
        setDefaultsOnInsert: true,
        useFindAndModify: false,
      });
    });

    return await Promise.all(promiseArray);
  }

  formatArticle(article: any) {
    const result = {};
    for (const i in article) {
      result[camelCase(i)] = article[i];
    }
    return result;
  }
  @Cron(CronExpression.EVERY_HOUR)
  async fetchArticlesHourly() {
    await this.updateArticles();
  }

  @Timeout(5000)
  async fetchArticlesOnStartup() {
    await this.updateArticles();
  }
}
